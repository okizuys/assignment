package nl.rabobank.mongo.repositories;

import nl.rabobank.mongo.MongoConfiguration;
import nl.rabobank.mongo.entity.account.Account;
import nl.rabobank.mongo.entity.authorization.AccountAuthorization;
import nl.rabobank.mongo.entity.authorization.Authorization;
import nl.rabobank.mongo.repositories.account.AccountRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = MongoConfiguration.class)
@DataMongoTest
class AccountRepositoryTest {

  @Autowired private AccountRepository accountRepository;
  @Autowired private AuthorizationRepository authorizationRepository;

  @Test
  void testCreate() {
    String employee = "grantee1";
    Account account = createAccount("NL123456", "Holder Account 1");
    Account account2 = createAccount("NL123456789", "Holder Account 2");

    createAuthorization(account, Authorization.WRITE, employee);
    createAuthorization(account2, Authorization.WRITE, "grantee2");

    List<Account> result = accountRepository.findAllForEmployee(employee);

    assertThat(result).hasSize(1);
    assertThat(result.get(0)).isEqualTo(account);
  }

  private Account createAccount(String number, String holder) {
    Account account = new Account();
    account.setAccountNumber(number);
    account.setAccountHolderName(holder);
    return accountRepository.save(account);
  }

  private AccountAuthorization createAuthorization(
      Account account, Authorization authorization, String grantee) {
    AccountAuthorization accountAuthorization = new AccountAuthorization();
    accountAuthorization.setAccountNumber(account.getAccountNumber());
    accountAuthorization.setGrantee(grantee);
    accountAuthorization.setGrantor("grantor1");
    accountAuthorization.setAuthorization(authorization);
    return authorizationRepository.save(accountAuthorization);
  }
}
