package nl.rabobank.mongo.repositories.account;

import nl.rabobank.mongo.entity.account.Account;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends MongoRepository<Account, ObjectId>, AccountCustomRepository {

}
