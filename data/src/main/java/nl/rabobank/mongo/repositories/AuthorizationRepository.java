package nl.rabobank.mongo.repositories;

import nl.rabobank.mongo.entity.account.Account;
import nl.rabobank.mongo.entity.authorization.AccountAuthorization;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorizationRepository extends MongoRepository<AccountAuthorization, ObjectId> {
  Optional<Account> findByAccountNumber(String accountNumber);
}
