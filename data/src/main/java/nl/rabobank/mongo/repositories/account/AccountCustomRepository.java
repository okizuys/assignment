package nl.rabobank.mongo.repositories.account;

import nl.rabobank.mongo.entity.account.Account;

import java.util.List;

public interface AccountCustomRepository {

  List<Account> findAllForEmployee(String employee);
}
