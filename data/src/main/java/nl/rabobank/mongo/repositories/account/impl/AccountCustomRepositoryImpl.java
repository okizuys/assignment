package nl.rabobank.mongo.repositories.account.impl;

import nl.rabobank.mongo.entity.account.Account;
import nl.rabobank.mongo.repositories.account.AccountCustomRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountCustomRepositoryImpl implements AccountCustomRepository {
  private static final String AUTHORIZATIONS_LOOKUP_COLLECTION = "authorizations";
  private static final String LOOKUP_LOCAL_FIELD = "accountNumber";
  private static final String FOREIGN_FIELD = "accountNumber";
  private static final String ALIAS = "account_access";

  private final MongoTemplate mongoTemplate;

  public AccountCustomRepositoryImpl(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @Override
  public List<Account> findAllForEmployee(String employee) {
    LookupOperation lookup =
        LookupOperation.newLookup()
            .from(AUTHORIZATIONS_LOOKUP_COLLECTION)
            .localField(LOOKUP_LOCAL_FIELD)
            .foreignField(FOREIGN_FIELD)
            .as(ALIAS);
    Aggregation aggregation =
        Aggregation.newAggregation(
            lookup, Aggregation.match(Criteria.where("account_access.grantee").is(employee)));
    return mongoTemplate.aggregate(aggregation, Account.class, Account.class).getMappedResults();
  }
}
