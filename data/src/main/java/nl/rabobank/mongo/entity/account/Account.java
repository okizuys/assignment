package nl.rabobank.mongo.entity.account;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Setter
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Document(collection = "accounts")
public class Account {
  @Id private ObjectId objectId;

  @EqualsAndHashCode.Include
  @Field("accountNumber")
  private String accountNumber;

  @Field("accountHolderName")
  private String accountHolderName;

  @Field("balance")
  private Double balance;

  @Field("type")
  private AccountType type;
}
