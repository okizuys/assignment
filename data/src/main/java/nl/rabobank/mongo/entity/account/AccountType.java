package nl.rabobank.mongo.entity.account;

public enum AccountType {
    PAYMENT,
    SAVING
}
