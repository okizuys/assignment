package nl.rabobank.mongo.entity.authorization;

public enum Authorization {
  READ,
  WRITE
}
