package nl.rabobank.mongo.entity.authorization;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "authorizations")
public class AccountAuthorization {
  @Id private ObjectId id;

  @Field("grantee")
  private String grantee;

  @Field("grantor")
  private String grantor;

  @Field("accountNumber")
  private String accountNumber;

  @Field("authorization")
  private Authorization authorization;
}
