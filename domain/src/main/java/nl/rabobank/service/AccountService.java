package nl.rabobank.service;

import lombok.RequiredArgsConstructor;
import nl.rabobank.account.Account;
import nl.rabobank.mapper.AccountEntityToDomainModelMapper;
import nl.rabobank.mongo.repositories.account.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class AccountService {

  private final AccountRepository accountRepository;
  private final AccountEntityToDomainModelMapper mapper;

  public List<Account> getForEmployee(String employee) {

    return accountRepository.findAllForEmployee(employee).stream()
        .map(mapper)
        .collect(Collectors.toList());
  }
}
