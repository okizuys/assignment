package nl.rabobank.service;

import lombok.RequiredArgsConstructor;
import nl.rabobank.authorizations.PowerOfAttorney;
import nl.rabobank.mapper.PowerOfAttorneyToAccountAuthorizationMapper;
import nl.rabobank.mongo.repositories.AuthorizationRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class AuthorizationService {

  private final AuthorizationRepository authorizationRepository;
  private final PowerOfAttorneyToAccountAuthorizationMapper mapper;

  public void create(PowerOfAttorney powerOfAttorney) {
    authorizationRepository.save(mapper.apply(powerOfAttorney));
  }
}
