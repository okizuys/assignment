package nl.rabobank.mapper;

import nl.rabobank.authorizations.PowerOfAttorney;
import nl.rabobank.mongo.entity.authorization.AccountAuthorization;
import nl.rabobank.mongo.entity.authorization.Authorization;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class PowerOfAttorneyToAccountAuthorizationMapper
    implements Function<PowerOfAttorney, AccountAuthorization> {

  @Override
  public AccountAuthorization apply(PowerOfAttorney powerOfAttorney) {
    AccountAuthorization accountAuthorization = new AccountAuthorization();
    accountAuthorization.setAuthorization(
        Authorization.valueOf(powerOfAttorney.getAuthorization().name()));
    accountAuthorization.setAccountNumber(powerOfAttorney.getAccount().getAccountNumber());
    accountAuthorization.setGrantor(powerOfAttorney.getGrantorName());
    accountAuthorization.setGrantee(powerOfAttorney.getGranteeName());
    return accountAuthorization;
  }
}
