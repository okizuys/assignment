package nl.rabobank.mapper;

import nl.rabobank.account.Account;
import nl.rabobank.account.PaymentAccount;
import nl.rabobank.account.SavingsAccount;
import nl.rabobank.mongo.entity.account.AccountType;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AccountEntityToDomainModelMapper
    implements Function<nl.rabobank.mongo.entity.account.Account, Account> {
  @Override
  public Account apply(nl.rabobank.mongo.entity.account.Account source) {
    if (source.getType() == AccountType.PAYMENT) {
      return new PaymentAccount(
          source.getAccountNumber(), source.getAccountHolderName(), source.getBalance());
    }
    return new SavingsAccount(
        source.getAccountNumber(), source.getAccountHolderName(), source.getBalance());
  }
}
