package nl.rabobank.controller;

import nl.rabobank.mapper.AccountEntityToDomainModelMapper;
import nl.rabobank.model.authorization.AuthorizationRequest;
import nl.rabobank.mongo.entity.account.Account;
import nl.rabobank.mongo.repositories.AuthorizationRepository;
import nl.rabobank.service.AuthorizationService;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AuthorizationControllerTest {

  private final AuthorizationService authorizationService = mock(AuthorizationService.class);
  private final AuthorizationRepository authorizationRepository =
      mock(AuthorizationRepository.class);
  private final AccountEntityToDomainModelMapper accountMapper =
      mock(AccountEntityToDomainModelMapper.class);

  private final AuthorizationController authorizationController =
      new AuthorizationController(authorizationService, authorizationRepository, accountMapper);

  @Test
  void testGrant_whenNoAccountByAccountNumber_thenException() {

    AuthorizationRequest request = new AuthorizationRequest();
    request.setAccountNumber("NL123456789");

    when(authorizationRepository.findByAccountNumber(request.getAccountNumber()))
        .thenReturn(Optional.empty());

    assertThatThrownBy(() -> authorizationController.grant(request))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void testGrant_whenExistsAccountByAccountNumber_thenNoException() {

    AuthorizationRequest request = new AuthorizationRequest();
    request.setAccountNumber("NL123456789");

    when(authorizationRepository.findByAccountNumber(request.getAccountNumber()))
        .thenReturn(Optional.of(new Account()));

    authorizationController.grant(request);
  }
}
