package nl.rabobank.model.authorization;

import lombok.Data;
import nl.rabobank.authorizations.Authorization;

import javax.validation.constraints.NotNull;

@Data
public class AuthorizationRequest {

  @NotNull private String granteeName;
  @NotNull private String grantorName;
  @NotNull private String accountNumber;
  @NotNull private Authorization authorization;
}
