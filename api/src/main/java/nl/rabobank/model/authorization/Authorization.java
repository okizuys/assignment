package nl.rabobank.model.authorization;

public enum Authorization {
  READ,
  WRITE
}
