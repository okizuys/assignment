package nl.rabobank.model.account;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AccountRequest {
  @NotNull
  private String accountNumber;
  @NotNull
  private String accountHolderName;
}
