package nl.rabobank.model.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponse {

  private String accountNumber;
  private String accountHolderName;
  private Double balance;
}
