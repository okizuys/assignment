package nl.rabobank.controller;

import lombok.RequiredArgsConstructor;
import nl.rabobank.authorizations.PowerOfAttorney;
import nl.rabobank.mapper.AccountEntityToDomainModelMapper;
import nl.rabobank.model.authorization.AuthorizationRequest;
import nl.rabobank.mongo.entity.account.Account;
import nl.rabobank.mongo.repositories.AuthorizationRepository;
import nl.rabobank.service.AuthorizationService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/authorizations")
public class AuthorizationController {

  private final AuthorizationService authorizationService;
  private final AuthorizationRepository authorizationRepository;
  private final AccountEntityToDomainModelMapper accountMapper;

  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping
  public void grant(@Valid @RequestBody AuthorizationRequest request) {
    Account account =
        authorizationRepository
            .findByAccountNumber(request.getAccountNumber())
            .orElseThrow(() -> new IllegalArgumentException("Invalid account number"));

    authorizationService.create(
        PowerOfAttorney.builder()
            .authorization(request.getAuthorization())
            .account(accountMapper.apply(account))
            .granteeName(request.getGranteeName())
            .grantorName(request.getGrantorName())
            .build());
  }
}
