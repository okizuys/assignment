package nl.rabobank.controller;

import lombok.RequiredArgsConstructor;
import nl.rabobank.account.Account;
import nl.rabobank.model.account.AccountResponse;
import nl.rabobank.service.AccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/accounts")
public class AccountController {

  private final AccountService accountService;

  @GetMapping("employees/{employee}")
  public List<AccountResponse> get(@PathVariable String employee) {
    List<Account> accounts = accountService.getForEmployee(employee);
    return accounts.stream()
        .map(
            it ->
                new AccountResponse(
                    it.getAccountNumber(), it.getAccountHolderName(), it.getBalance()))
        .collect(Collectors.toList());
  }
}
